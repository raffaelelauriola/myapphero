import {
  Injectable
} from '@angular/core';
import {
  Hero
} from '../models/hero';
import {
  Observable,
  of
} from 'rxjs';
import {
  MessagesService
} from './messages.service';
import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http';
import {
  catchError,
  map,
  tap
} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HeroService {
  private heroesUrl = 'api/heroes';

  private log(message: string) {
    this.messagesServices.add(`HeroService: ${message}`);
  }
  private handleError < T > (operation = 'operation', result ? : T) {
    return (error: any): Observable < T > => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  constructor(private messagesServices: MessagesService, private http: HttpClient) {}

  getHeroes(): Observable < Hero[] > {
    this.messagesServices.add('HeroService: fetched heroes');
    return this.http.get < Hero[] > (this.heroesUrl).pipe(catchError(this.handleError('getHeroes', [])));
  }

  getHero(id: number): Observable < Hero > {
    const url = `${this.heroesUrl}/${id}`;
    return this.http.get < Hero > (url).pipe(
      tap(_ => this.log(`fetched hero id=${id}`)),
      catchError(this.handleError < Hero > (`getHero id=${id}`))
    );
  }

  updateHero(hero: Hero): Observable < any > {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.put(this.heroesUrl, hero, httpOptions).pipe(tap(_ => this.log(`updated hero id=${hero.id}`)), catchError(this.handleError < any > ('updated hero')));
  }

  addHero(hero: Hero): Observable < Hero > {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(this.heroesUrl, hero, httpOptions).pipe(tap((hero: Hero) => this.log(`added hero id=${hero.id}`)), catchError(this.handleError < any > ('added hero')));
  }

  deleteHero(hero: Hero | number): Observable < Hero > {
    const id = typeof hero === 'number' ? hero : hero.id;
    const url = `${this.heroesUrl}/${id}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.delete(url,httpOptions).pipe(tap(_=>this.log(`deleted hero id=${id}`)),catchError(this.handleError < any > ('deleted hero')));
  }

  searchHero(term:string):Observable<Hero[]>{
    if (!term.trim()) {
      return of([]);
    }
    return this.http.get<Hero[]>(`${this.heroesUrl}/?name=${term}`).pipe(tap(_=>this.log(`searched hero term=${term}`)),catchError(this.handleError<Hero[]>('searched hero',[])));
  }

}
